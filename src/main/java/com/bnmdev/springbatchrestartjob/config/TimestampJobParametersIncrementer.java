package com.bnmdev.springbatchrestartjob.config;

import java.util.Date;

import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersIncrementer;
import org.springframework.lang.Nullable;

public class TimestampJobParametersIncrementer implements JobParametersIncrementer {
	
	private static String RUN_ID_KEY = "timestamp";

	@Override
	public JobParameters getNext(@Nullable JobParameters parameters) {
		JobParameters params = new JobParameters();
		long id = new Date().toInstant().toEpochMilli();
		return new JobParametersBuilder(params).addLong(RUN_ID_KEY, id).toJobParameters();
	}
}
