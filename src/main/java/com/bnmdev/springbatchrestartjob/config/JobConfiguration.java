package com.bnmdev.springbatchrestartjob.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing
public class JobConfiguration {
	
	@Autowired
	private JobBuilderFactory jobBuilderFactory;
	@Autowired
	private StepBuilderFactory steps;
	@Autowired
	private Tasklet tasklet;
	
	@Bean
	public Job job() {
		new RunIdIncrementer();
		return this.jobBuilderFactory.get("restartableJob")
				.incrementer(new TimestampJobParametersIncrementer())
				.start(step1())
				.build();
	}

	@Bean
	public Step step1() {
		return this.steps.get("step1")
				.tasklet(tasklet)
				.build();
	}
}
