package com.bnmdev.springbatchrestartjob;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbatchRestartJobApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbatchRestartJobApplication.class, args);
	}

}
